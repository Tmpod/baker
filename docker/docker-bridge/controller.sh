#!/bin/sh
# -*- coding: utf-8 -*-
# Colours
RED="\e[1;31m"
GREEN="\e[1;32m"
GREY="\e[1;30m"
YELLOW="\e[1;33m"
GOLD="\e[0;33m"
NC="\e[0m"

# Colourful logging
log_error() {
    echo -e "${RED}ERROR: $1${NC}"
}

log_info() {
    echo -e "${GREY}INFO: $1${NC}"
}

log_success() {
    echo -e "${GREEN}$1${NC}"
}

REBOOT_FILE="/docker-bridge/REBOOT"

build() {
    if [[ $ENV_TYPE == "PROD" ]]; then
        git clone https://$GIT_USERNAME:$GIT_TOKEN@$GIT_URL /src
        cd /src 
        git gc --aggressive --prune=NOW
        git checkout rewrite 

        REQS_PATH=/src/requirements.txt
    else
        REQS_PATH=/docker-bridge/requirements.txt
    fi

    pip install --upgrade pip 
    pip install -U -r $REQS_PATH
}

launch() {
    cd /src
    
    # if [[ $PRODUCTION =~ ^("true"|"yes"|"1")$ ]]; then
    if [[ $ENV_TYPE == "PROD" ]]; then
        if [[ -f $REBOOT_FILE ]]; then
            log_info "REBOOT file found! Now reading reboot options..."
            let counter=0 # start counter
            while IFS='' read -r line || [[ -n "$line" ]]; do # read lines one by one

                [ ${counter} -eq 0 ] && [[ ${line} = "1" ]] \
                    && log_info "Updating the local repo..." \
                    && git checkout rewrite \
                    && git pull \
                    && log_info "Pulled!"

                [ ${counter} -eq 0 ] && [[ ${line} = "0" ]] \
                    && log_info "No update task detected! Skipping..."

                [ ${counter} -eq 1 ] && [[ ${line} = "1" ]] \
                    && log_info "Update dependencies order detected! Executing order..." \
                    && pip install -U -r requirements.txt --user \
                    && log_info "Dependencies updated!"

                [ ${counter} -eq 1 ] && [[ ${line} = "0" ]] \
                    && log_info "Skipping dependencies update..."

                counter=$((counter+1))
            done < "$REBOOT_FILE" 

        else
            log_info "No REBOOT file found! Skipping..."
        fi

    else
        log_info "Development environment active. Using local repo!"
    fi

    log_info "Proceeding to launch the bot..."
    python -u -m app $1 $2
}

cmd=$1
case $cmd in 
    build|b)
        build
        ;;

    launch|l) 
        launch $CONFIG_FILE $INIT_LOG_LEVEL
        ;;

    *)
        log_error "> $cmd < isn't a valid command! Only > clone < or > launch <"
        ;;
esac
