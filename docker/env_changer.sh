#!/bin/sh
# -*- coding: utf-8 -*-
# Colours
RED="\e[1;31m"
GREY="\e[1;30m"
GREEN="\e[1;32m"
NC="\e[0m"

# Colourful logging
log_error() {
    echo -e "${RED}ERROR: $1${NC}"
}

log_info() {
    echo -e "${GREY}INFO: $1${NC}"
}

log_success() {
    echo -e "${GREEN}$1${NC}"
}

development() {
	log_info "Changing environment to DEVELOPMENT..."
	if [[ -f "docker-compose-P.yml" ]]; then 
		log_info "Docker Compose file already set!"
	else
		mv docker-compose.yml docker-compose-P.yml \
		&& mv docker-compose-D.yml docker-compose.yml \
		&& log_info "Switched Docker Compose files."
	fi

	cp local-src/requirements.txt docker-bridge/requirements.txt \
	&& log_info "Updated the requirements.txt file on the Docker bridge"
}

production() {
	log_info "Changing environment to PRODUCTION..."
	if [[ -f "docker-compose-D.yml" ]]; then
		log_info "Docker Compose file already set!"
	else		
		mv docker-compose.yml docker-compose-D.yml
		mv docker-compose-P.yml docker-compose.yml
		log_info "Switched Docker Compose files."
	fi

}


cmd=$1
case $cmd in 
	production|p|prod)
		production \
			&& log_success "Successfully changed the environment to PRODUCTION!" \
			|| log_error "Something went wrong when changing the environment to PRODUCTION!"
		;;

	development|d|dev)
		development \
			&& log_success "Successfully changed the environment to DEVELOPMENT!" \
			|| log_error "Something went wrong when changing the environment to DEVELOPMENT!"
		;;

	*)
		log_error "Invalid command! This script only supports > production < and > development < environments."
		;;
esac