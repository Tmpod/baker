# Running the app with Docker

There's two ways of running the app with Docker: using a [remote repositoy](#using-a-remote-repo) or a [local repository](#using-a-local-repo).

## Using a remote repo

To use a remote repo you gotta get your GitLab username and a `read_repository` [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). Then run `bash env_changer.sh prod` and go on the `docker-compose.yml` file and put your GitLab creds on the `GITLAB_USERNAME` and `GITLAB_TOKEN` environment vars.

## Using a local repo

Clone the app repo into `local-src` (`git clone <url> local-src`) and then run `bash env_changer dev`. After all that's done, you should be able to just use `local-src` as your local development folder and do `docker-compose up` to run the app (add the `-d` flag to run the services in detached mode).


