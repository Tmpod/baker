#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

__all__ = ("BotClient",)


import aiohttp
from asyncio import sleep as asleep
from concurrent import futures
from datetime import timedelta
from discord import Activity, ActivityType, Game, Streaming
from discord import __version__ as __dpy_version__
from functools import partial as fpartial
import json
from libneko.asyncinit import AsyncInit
from libneko import clients
import logging
from random import choice
from time import perf_counter

from . import __version__ as __baker_version__
from .modules.secrets_handler import BakerConfig

from app import __version__ as __app_version__
from app.utils import run_in_shell


class BotClient(clients.Bot, AsyncInit):
    """
    The bot class. This is slightly modified to house some neat features
    """

    def __init__(self, *, config_data: BakerConfig, baker, **kwargs):
        super().__init__(**kwargs)
        # self.client_id = config_data.toast.client_id
        # self.owner_id = config_data.toast.owner_id

        self.logger = logging.getLogger(config_data.get("logger_name", "BotClient"))
        self.logger.setLevel(config_data.get("logger_level", logging.INFO))

        self.logger.info("Invoked!")

        self.baker = baker
        self.config = config_data
        self._token = self.config.token
        # self.default_prefixes = self.config.toast.default_prefixes
        self.started_at = perf_counter()
        self.tpe = futures.ThreadPoolExecutor()
        self.failed_exts_on_startup = {}
        self.failed_exts = {}
        self.loc = None
        self.cmd_counter = 0

        self.logger.info("Starting...")

    async def __ainit__(self, *, config_data: BakerConfig, baker, **kwargs):
        # Setting the description
        desc_location = self.baker.sec.data.baker.get("custom_description")

        if desc_location:
            async with await self.baker.rsc.request("GET", desc_location) as fp: 
                self.description = (await fp.read()).strip()
        else:
            self.description = "A Discord bot"

        await self._init_extentions()

        self._init_activities()

        # Initial LOC counting and caching
        await self.count_loc()

    async def _init_extensions(self):
        """Initial extention loading"""
        # Getting and processing the extensions file
        exts = (await self.baker.rsc.get("extensions.txt")).strip().split("\n")

        self.default_exts = (
            f"app.exts.{l.strip()[1:]}" if l.startswith("*") else l.strip()
            for l in exts
            if l and not l.startswith("#")
        )
        ### Expanded ###
        # self.default_exts = []
        # for line in exts_file:
        #     line = line.strip()
        #     if not line or line.startswith('#'):
        #         continue
        #     if line.startswith('*'):
        #         line = 'app.exts.' + line[1:]
        #     self.default_exts.append(line)

        # Now loading them
        for exts in self.default_exts:
            try:
                self.load_extension(exts)
                self.logger.info(f"Loaded: {exts}")
            except BaseException as ex:
                self.logger.error(f"Could not load {exts}\n%s", ex, exc_info=True)
                self.failed_exts_on_startup[exts] = ex

    def _init_activities(self):
        """Activity loading"""
        self._activities = None
        try:
            act_path = self.baker.sec.data.bot.get("activities")
            if act_path:
                self._activities = self.baker.mdl.get_module_extension(
                    act_path
                ).ACTIVITIES
        except ImportError as ex:
            self.logger.error(
                f"Could not load the activities messages with\n%s",
                ex,
                "\nProceeding with the plan-B, hard-coded message.",
                exc_info=True,
            )

    async def _change_activities(self):
        await self.wait_until_ready()
        # Checking if the activities were Successfully loaded
        if not self._activities:
            self.logger.warning(
                "Could not load activities! Proceeding with the default one..."
            )
            # If not queue the default activity
            await self.change_presence(activity=Game(name="bits and bytes"))
            return

        # Simple 'decoder'
        act_decoder = {
            "watch": lambda n: Activity(
                type=ActivityType.watching, name=n if not callable(n) else n(self)
            ),
            "play": lambda n: Game(name=n if not callable(n) else n(self)),
            "listen": lambda n: Activity(
                type=ActivityType.listening, name=n if not callable(n) else n(self)
            ),
            "stream": lambda n: Streaming(
                url=self.config.streaming_url, name=n if not callable(n) else n(self)
            ),
        }
        # Creating the loop
        self.logger.info(
            "Activities found and successfully loaded! Creating the activity changer loop..."
        )
        while True:
            act_type = choice([*self._activities.keys()])
            possb = choice(self._activities[act_type])
            await self.change_presence(activity=act_decoder[act_type](possb))
            # Wait until the next change
            await asleep(self.config.get("activity_timer", 30))

    async def run_in_tpe(self, func, *args, **kwargs):
        """Runs synchronous functions on the loop executor pool"""
        partial = fpartial(func, *args, **kwargs)
        return await self.loop.run_in_executor(self.tpe, partial)

    async def size(self) -> str:
        """
        Returns the size of the bot's dir.
        Uses a shell subprocess to execute 'du -sh'
        One-liner of:
        proc = await create_subprocess_shell("du -sh", stdout=aioPIPE, stderr=aioPIPE)
        stdout, stderr = await proc.communicate()
        size = stdout.decode[:8] # removing the tab, and the path
        return f"{size}B" # adding a B for byte
        """
        return f"{(await run_in_shell('du -sh')).split()[0]}B"

    async def count_loc(self):
        """
        Counts the lines of code.
        """
        try:
            self.logger.info("Counting and caching LOC.")
            # Runs cloc with JSON output
            raw_loc = await run_in_shell(
                "cloc --json --exclude-dir=__pycache__,venv,.idea,cache /bakery"
            )
            # Gets the number from the total line of the output for cloc
            self.loc = json.loads(raw_loc)
        finally:  # Not sure about this...
            return

    def run(self):
        super().run(self._token)

    async def start(self, token: str = None, *, bot: bool = True):
        task = None
        try:
            task = self.loop.create_task(self._change_activities())
            task.add_done_callback(lambda _: task.result())
            await super().start(token or self._token, bot=bot)
        finally:
            if task is not None:
                task.cancel()

    async def logout(self):
        """Adds a new event when the bot logs out"""
        self.dispatch("logout")
        await super().logout()

    async def on_ready(self):
        """Event listener for when the bot is ready to go"""
        self.owner = (await self.application_info()).owner

        banner = await self.baker.rsc.get("ready_banner.txt")

        replacements = {
            "ID": self.user.id,
            "OWNER": str(self.owner),
            "VERSION": __app_version__,
            "BAKER_VERSION": __baker_version__,
            "DPY_VERSION": __dpy_version__,
        }

        print(banner.format(**replacements))

    @property
    def uptime(self) -> timedelta:
        """Returns the bot's uptime"""
        return timedelta(seconds=perf_counter() - self.started_at)

    @property
    def client_session(self):
        """Returns an aiohttp ClientSession"""
        try:
            cs = getattr(self, "__client_session")
        except AttributeError:
            cs = aiohttp.ClientSession()
            setattr(self, "__client_session", cs)
        finally:
            return cs
