#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

__all__ = ("BakerRunner",)

from asyncio import set_event_loop_policy, get_event_loop
from libneko.logging import Log
import logging
from sys import argv as sys_argv

from app.baker.baker import Baker
from app.baker.threadweeblogger import ThreadedLoggerWebhookHandler


class BakerRunner(Log):
    # __slots__ = ("loop", "baker")

    def __init__(self, args=sys_argv, *, loop=None):
        self.loop = loop or get_event_loop()

        log_format = "{asctime}.{msecs:03.0f} | {levelname:^8} | {name}::{message}"
        date_format = "%Y.%m.%d %H.%M.%S"

        if len(args) == 3:
            log_lvl = args[2]
            # Setting a temporary logging level
            logging.basicConfig(
                level=log_lvl,
                # filename=log_file,
                style="{",
                format=log_format,
                datefmt=date_format,
            )

        root_lg = logging.getLogger("")

        logger = logging.getLogger("bakerrunner")

        # Setting the event loop policy to uvloop which is faster
        logger.info("Attempting to set uvloop as the event loop policy...")
        try:
            from uvloop import EventLoopPolicy

            set_event_loop_policy(EventLoopPolicy())
            del EventLoopPolicy
        except ModuleNotFoundError:
            logger.warning("Could not load uvloop! Skipping...")
        else:
            logger.info("Using uvloop for asyncio event loop policy.")

        logger.info(
            "BAKERRUNNER has started and is now going to check for a config file..."
        )

        # Checking for a config file
        if len(args) < 2:
            logger.critical("No config file was passed. Cannot continue without it!")
            raise RuntimeError("Provide a config file as an argument!")

        sec_path = args[1]

        # Baker initialization
        logger.info("Starting Baker!")
        self.baker = self.loop.run_until_complete(Baker(self, secrets=sec_path))

        secrets = self.baker.sec.data

        logging_config = secrets.get("logging")
        if logging_config:
            # Setting a more concrete basic config
            logging.basicConfig(
                level=logging_config.get("level", log_lvl),
                style=logging_config.get("style", "{"),
                format=logging_config.get("format", log_format),
                datefmt=logging_config.get("datefmt", date_format),
            )

            if logging_config.get("webhook"):
                log_weeb = ThreadedLoggerWebhookHandler(**secrets.logging.webhook)

            # Adding the Webhook flusher handler to the root logger
            # should be enough to cover everything
            root_lg.addHandler(log_weeb)

    def run(self):
        """Starting the whole process"""
        self.baker.start()
