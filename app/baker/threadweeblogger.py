#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""
Thread-based webhooking logging handler to dump bot logs asynchronously to a logger channel.

This is done entirely separately to the bot's main connection, so if the bot goes down, we
should still get logs! This has a potentially noticable latency, as it will wait for buffers to
fill up to the size of a discord message or around 10 seconds before sending any logs. This
prevents us hitting ratelimits inside Discord's webhook system which silently drops our logs.

Made by Espy/Ko Yagami
Slighly modified by Tmpod, but all credit goes to him either way
"""
import datetime
import logging
import queue
import threading
import time

import requests
from discord.ext.commands import Paginator


MAX_MESSAGE_LEN = 1998

# noinspection PyProtectedMember
class ThreadedLoggerWebhookHandler(threading.Thread, logging.Handler):
    MAX_TIME_BEFORE_POST_RECORD = 10

    def __init__(self, url: str, name: str = "Logs", page_prefix: str = "", page_suffix: str = ""):
        threading.Thread.__init__(self, name=name, daemon=True)
        self.url = url
        self.name = name
        self.session = requests.Session()
        self.queue = queue.Queue()
        self.pag = Paginator(prefix=page_prefix, suffix=page_suffix)
        self.last_flush_time = time.perf_counter()
        logging.Handler.__init__(self)
        self.start()

    def emit(self, record):
        if record.levelno >= logging.INFO:
            created = datetime.datetime.fromtimestamp(record.created)
            record = f"`{created} - {self.format(record).replace('`', '′')}`"
            self.queue.put_nowait(record)

    def _flush(self):
        self.last_flush_time = time.perf_counter()
        pages = self.pag._pages
        if len(pages) > 1:
            pages_to_send = pages[:-1]
            remaining = [pages[-1]]
        else:
            # Causes the last page to be closed, causing it to be formatted correctly.
            pages = self.pag.pages
            pages_to_send = pages
            remaining = []

        self.pag._pages = remaining

        for page in pages_to_send:
            if page.strip():
                self.session.post(self.url, data={"username": self.name, "content": page})

    def _maybe_flush(self):
        enough_pages = len(self.pag._pages) > 1
        if enough_pages:
            self._flush()

    def run(self):
        while True:
            try:
                record = self.queue.get(block=True, timeout=self.MAX_TIME_BEFORE_POST_RECORD)
                if record is None:
                    continue

                self.pag.add_line(record[:MAX_MESSAGE_LEN])
                self._maybe_flush()
            except queue.Empty:
                self._flush()
            except Exception:
                logging.exception("Disconnecting entire bot")
