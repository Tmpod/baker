#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

__all__ = ("Baker",)

from asyncio import get_event_loop
from discord.errors import LoginFailure
from libneko.asyncinit import AsyncInit
from libneko.funcmods import is_coroutine_function
import logging
from typing import Iterable

from .modules.module_handler import ModuleHandler
from .modules.secrets_handler import SecretsHandler
from .mold import BotClient
from .threadweeblogger import ThreadedLoggerWebhookHandler

# pylint: disable=E1101
# That disables the warning for the modules that get loaded on runtime

# TODO: Doc improvements

class Baker(AsyncInit):
    """
    A discord.py bot backend/engine meant to assist in bot developers in the process of
    making a Discord bot with Python. This provides a modular interface to which you can
    attach useful parts to make your bot sit on a sturdy backend. This also tries to 
    split and decentralise stuff, removing some weight from the Discord client.
    Originally made for CyborgToast and the idea was based around Requiem's Cardinal.
    """

    # Sets perform amazingly well when it comes to checking if it contains something
    VALID_EVENTS = {"init", "start", "stop"}

    def __init__(self, runner: ".runner.BakerRunner", secrets: str, *, loop=None):
        """Sync init. This will just set the logger and some other stuff"""
        self.logger = logging.getLogger("baker")
        self.logger.info("Invoked!")

        self.runner = runner
        self.loop = loop or get_event_loop()
        self._secrets_path = secrets

        self.listener_pool = {e: [] for e in self.VALID_EVENTS}

    async def __ainit__(self, runner: ".runner.BakerRunner", secrets: str, *, loop=None):
        """Baker initial run... performs all system startup checks as well as initializing the bot instance"""
        self.logger.info("Baker is beginning its STARTUP RUN!")

        self.mdl = ModuleHandler(self)
        self.mdl.add_module(SecretsHandler(self._secrets_path))

        for m in self.sec.data.baker.modules:
            await self.mdl.load_extension(f"app.baker.modules.{m}")

        # Dispatch 'on_init'
        self.dispatch_event("init", secrets, loop=loop)

        # Instanciate the bot client class
        self.bot = await BotClient(
            config_data=self.sec.data.bot,  # pylint: disable=E1101
            baker=self,
            command_prefix=self.sec.data.bot.prefix,
            case_insensitive=self.sec.data.bot.get("case_insensitive", True),  
        )

        self.logger.info("Baker finished its STARTUP RUN!")

    def start(self):
        """Starts the bot client"""
        self.dispatch_event("start")

        self.logger.info("Initializing the BOT service...")
        while not self.sec.data.bot.get("token"):
            self.logger.critical(
                "BAKER HASN'T FOUND A TOKEN! CHECK IF THE CONFIG FILE IS CORRECT! HIT ENTER TO RETRY..."
            )
            input()
            self.sec.load()

        try:
            self.logger.info("Running the bot now!")
            self.bot.run()
        except LoginFailure:
            self.logger.critical(
                "INVALID TOKEN DETECTED! CHECK IF THE CONFIG FILE IS CORRECT! HIT ENTER TO RETRY..."
            )
            input()
            self.sec.load()

    async def stop(self):
        """Stops the app and exits the process"""
        self.dispatch_event("logout")
        await self.bot.logout()

    def dispatch_event(self, event: str, *args, **kwargs):
        """Dispatches the specified event"""
        event_listeners = self.listener_pool[event]

        for e in event_listeners:
            self.loop.create_task(e(*args, **kwargs))

        self.logger.debug("Dispatched '%s' event", event)

    def add_event(self, event: str):
        """Adds a new event to the pool of valid names"""
        new_event = event[3:] if event.startswith("on_") else event
        self.VALID_EVENTS.add(new_event)

        self.logger.debug("Added new event type: %s", new_event)

    def remove_event(self, event: str):
        """Removes an event from the pool of valid names"""
        old_event = event[3:] if event.startswith("on_") else event
        self.VALID_EVENTS.remove(old_event)

        self.logger.debug("Removed event type: %s", old_event)

    def _validate_event_name(self, event: str) -> str:
        """This validates an possible event name and strips the 'on_' part"""
        if not (event in self.VALID_EVENTS or (event.startswith("on_") and event[3:] in self.VALID_EVENTS)):
            raise ValueError(f"'{event}' is an invalid listener name!")

        # return event.replace("on_", "")
        return event[3:]

    def add_listener(self, func, event: str):
        """Register a coroutine as a listener for the specified event"""
        if not is_coroutine_function(func):
            raise TypeError("Listener must be coroutine functions!")

        event_name = self._validate_event_name(event)

        self.listener_pool[event_name].append(func)

        self.logger.debug("Registered '%s' as a listener for '%s'", func, event_name)

    def remove_listener(self, func, event: str):
        """Unregisters a function from the pool of listeners"""
        event_name = self._validate_event_name(event)

        try:
            self.listener_pool[event_name].remove(func)

            self.logger.debug("Unregistered '%s' as a listener for '%s'", func, event_name)
        except ValueError:  # Just resends a ValueError with a prettier message 
            raise ValueError(f"'{func}' isn't a registered listener for '{event_name}")

    def listen(self, event: str):
        """Decorator that wraps :meth:`.add_listener`"""
        def decor(func):
            self.add_listener(func, event)
            return func

        return decor
