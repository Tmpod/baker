#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

__all__ = ("ErrorHandler",)

import discord
from libneko import commands

import asyncio
import importlib
from libneko import embeds
from random import choice
from textwrap import dedent
import traceback

from . import Module
from app.utils import ColourConverter

#: Hue to fallback if no config is specified
DEFAULT_ERROR_HUE = 0xFF2B2B


class ErrorHandler(Module):
    __slots__ = ("resps", "channel", "report_errors", "error_hue")

    NAMESPACE = "err"

    def __init__(self, baker):
        self.baker = baker

        try:
            resps_path = self.baker.sec.data.baker.err.get("resps")
            if resps_path:
                self.resps = self.baker.mdl.get_module_extension(resps_path).RESPS
        except AttributeError:  # The spec will be None if no module is found for that path
            self.resps = None
            self.logger.warning(
                "No error responses resource detected! Moving on with plain resps..."
            )

        self.channel = None
        self.report_errors = self.baker.sec.data.baker.err.get("report_errors", False)
        self.error_hue = self.baker.sec.data.baker.err.get("error_hue", DEFAULT_ERROR_HUE)

        async def _finish_init():
            await asyncio.sleep(2)  # Wait til the bot var has been set
            self.channel = self.baker.bot.get_channel(self.baker.sec.data.logging.channel)

        self.baker.loop.create_task(_finish_init())

    async def _react_for_a_while(
        self,
        msg: discord.Message,
        emoji: str = "\N{BLACK QUESTION MARK ORNAMENT}",
        duration: int = 10,
    ) -> None:
        try:
            await msg.add_reaction(emoji)
            await asyncio.sleep(duration)
            await msg.remove_reaction(emoji, msg.guild.me)
        except (discord.Forbidden, discord.errors.NotFound):
            pass

    async def catch_error(self, ctx: commands.Context, error):
        if isinstance(error, commands.CommandNotFound):
            self.baker.bot.loop.create_task(self._react_for_a_while(ctx.message))
            return

        colour = self.error_hue
        if isinstance(error, UserWarning) and len(error.args) == 2:
            converter = ColourConverter()
            colour = await converter.convert(ctx, error.args[1])

        error = error.__cause__ or error
        if self.resps:
            try:
                module = error.__module__
            except AttributeError:
                module = "builtins"

            try:
                ans = self.resps[module][error.__class__.__name__]
                if isinstance(ans, list):
                    ans = choice(ans)
                caught = True

            except (KeyError, AttributeError):
                caught = False
                ans = choice(self.resps["general"])
        else:
            ans = "Whoopsey! That wasn't supposed to happen..."

        await self._respond_to_error(
            ctx, ans(error) if callable(ans) else ans, caught=caught, colour=colour
        )

        if self.report_errors and not caught:
            await self._file_error_report(ctx, error)

    async def _respond_to_error(
        self,
        ctx: commands.Context,
        ans: str,
        caught: bool = False,
        colour: discord.Colour = DEFAULT_ERROR_HUE,
    ):
        if not caught:
            desc = dedent(
                f"""
                `{ans}`

                :warning: Something went wrong! This has been reported to my creator.
                Sorry for the inconvenience!
                """
            )
        else:
            desc = ans
        await ctx.send(embed=embeds.Embed(description=desc, colour=colour))

    async def _file_error_report(self, ctx: commands.Context, error):
        error = error.__cause__ or error

        self.logger.error(error, exc_info=True)

        em = embeds.Embed(
            title=":page_with_curl: Error report",
            description=dedent(
                f"""
                ```py
                {"".join(traceback.format_exception(type(error), error, error.__traceback__))}
                ```
                """
            ),
        )
        em.add_field(
            name="More info",
            value=dedent(
                f"""
                __Guild:__ {ctx.guild} ({ctx.guild.id})
                __Channel:__ {ctx.channel} ({ctx.channel.mention})
                __Author:__ {ctx.author} ({ctx.author.id})
                __Message ID:__ {ctx.message.id}
                __Message content:__ {ctx.message.content}
                """
            ),
        )
        await self.channel.send(embed=em)


def setup(baker):
    baker.mdl.add_module(ErrorHandler(baker))
