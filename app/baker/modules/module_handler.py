#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

__all__ = ("ModuleHandler",)

import importlib
from libneko.funcmods import is_coroutine_function
from logging import getLogger

from os.path import basename as path_basename  # Ain't bothering with Win
import sys

from . import Module

"""
Handles module loading for Baker
"""

class ModuleHandler:
    """
    This is a handler for Baker that processes the loading and unloading of modules
    These modules need to extend from `baker.module.Module` and get the current Baker
    instance as well as a logger with the proper namespace. The namespace should be
    set in a field called `NAMESPACE`. If there's already a loaded module with the
    same namespace, an exception will be thrown.
    """
    __slots__ = ("logger", "baker", "loaded_extensions", "loaded_modules")

    NAMESPACE = "mdl"

    def __init__(self, baker):
        self.logger = getLogger(f"baker:{self.NAMESPACE}")
        self.baker = baker
        self.loaded_extensions = {}
        self.loaded_modules = {}

        # Setting Baker's attrs to point here
        self.baker.extensions = self.loaded_extensions
        self.baker.modules = self.loaded_modules

    @staticmethod
    def _inject_logger(obj):
        """Injects a logger object"""
        name = getattr(obj, "NAMESPACE") or obj.__class__.__name__.lower()
        # obj.logger = getLogger(name)  # not sure which one to choose
        setattr(obj, "logger", getLogger(f"baker.{name}"))  # I'll go with setattr for now ig

    def add_module(self, module):
        self.logger.debug("Adding module `%s`...", module)

        if not isinstance(module, Module):
            raise TypeError("Baker modules must extend from `baker.module.Module`!")

        if hasattr(self.baker, module.NAMESPACE):
            raise ValueError(f"Baker already has an attr called `{module.NAMESPACE}`!")

        setattr(module, "baker", self.baker)  # Injecting Baker
        self._inject_logger(module)
        setattr(self.baker, module.NAMESPACE, module)  # Setting this module's namespace
        self.loaded_modules[module.__class__.__name__] = module  # Register module

    def remove_module(self, name):
        self.logger.debug("Attempting to remove module named `%s`...", name)

        module = self.loaded_modules.pop(name, None)
        if module is None:
            self.logger.debug("Module `%s` wasn't found!", name)
        else:
            delattr(self.baker, module.NAMSPACE)
            self.logger.debug("Removed `%s`", name)

    def get_module_extension(self, module: str):
        """
        Gets a module by it's dot-notation path or file path
        """
        self.logger.debug("Going to try import module from `%s`", module)

        # (Crap) way to tell if it's using dot notaton or not
        if "/" in module or "\\" in module:
            spec = importlib.util.spec_from_file_location(
                path_basename(module).split(".")[0], module
            )
            return spec.loader.load_module()

        return importlib.import_module(module)

    async def load_extension(self, module: str):
        """
        Loads a Baker module
        You can either pass a file path or a dot-notation path
        """
        self.logger.debug("Loading module `%s`", module)

        if module in self.baker.modules:
            raise ValueError(f"The module `{module}` is already loaded!")

        lib = self.get_module_extension(module)
        # Checking if there's a setup function we can run
        if not hasattr(lib, 'setup'):
            del lib
            del sys.modules[module]
            raise ValueError(f"`{module}` doesn't contain a `setup` function!")

        # Passing Baker as the only argument
        if is_coroutine_function(lib.setup):
            await lib.setup(self.baker)
        else:
            lib.setup(self.baker)

        self.loaded_extensions[module] = lib

        self.logger.info("Loaded `%s`", module)

    async def unload_extension(self, module: str):
        """
        Unloads a Baker module
        You can either pass a file path or a dot-notation path
        """
        self.logger.debug("Unloading module `%s`", module)

        if module not in self.baker.modules:
            raise ValueError(f"The module `{module}` isn't loaded!")

        lib = self.loaded_extensions[module]

        if hasattr(lib, 'teardown'):
            try:
                if is_coroutine_function(lib.teardown):
                    await lib.teardown(self.baker)
                else:
                    lib.teardown(self.baker)
            except Exception:
                pass

        delattr(self.baker, self.loaded_extensions[module].NAMESPACE)
        del lib
        del self.loaded_extensions[module]
        del sys.modules[module]

        self.logger.info("Unloaded `%s`", module)
