#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

__all__ = ("SecretsHandler", "BakerConfig", "MissingRequiredConfig")

from libneko.aggregates import ImmutableProxy
from os.path import isfile
from typing import Mapping

from . import Module


class MissingRequiredConfig(Exception):
    """Exception from when a required config isn't found"""


# class BakerConfig(ImmutableProxy):
#     """
#     This is just a simple subclass that modifies some messages in order to make the interface
#     a bit nicer. No core behaviour is changed.
#     """

#     def __getattr__(self, item):
#         try:
#             return self.__dict[item]
#         except KeyError:
#             raise MissingRequiredConfig(
#                 f"{item} is a required config field that is missing!"
#             ) from None

#     def __getitem__(self, item):
#         try:
#             return self.__dict[item]
#         except KeyError:
#             raise MissingRequiredConfig(
#                 f"{item} is a required config field that is missing!"
#             ) from None

class BakerConfig(Mapping):
    """
    This is just a simple class that modifies some error messages in libneko's ImmutableProxy 
    in order to make the interface a bit nicer. No core behaviour is changed.
    I tried only subclassing but it kept raising some weird RecursionError's.
    """

    def __init__(self, kwargs, *, recurse=True):
        """
        If recurse is True, it converts all inner dicts to this type too.
        """
        if not isinstance(kwargs, dict):
            raise TypeError(f"Expected dictionary, got {type(kwargs).__name__}")

        if recurse:
            self.__dict = self._recurse_replace(kwargs)
        else:
            self.__dict = kwargs

    def __getattr__(self, item):
        try:
            return self.__dict[item]
        except KeyError:
            raise MissingRequiredConfig(
                f"{item} is a required config field that is missing!"
            ) from None

    def __getitem__(self, item):
        return self.__dict[item]
        
        # try:
        #     return self.__dict[item]
        # except KeyError:
        #     raise MissingRequiredConfig(
        #         f"{item} is a required config field that is missing!"
        #     ) from None

    def __len__(self) -> int:
        return len(self.__dict)

    def __iter__(self):
        return iter(self.__dict)

    def __str__(self):
        return str(self.__dict)

    def __repr__(self):
        return repr(self.__dict)

    def __hash__(self):
        return object.__hash__(self)

    @classmethod
    def _recurse_replace(cls, obj, already_passed=None):
        if already_passed is None:
            already_passed = []

        new_dict = {}
        for k, v in obj.items():
            if isinstance(v, dict):
                if v in already_passed:
                    new_dict[k] = v
                else:
                    already_passed.append(v)
                    new_dict[k] = cls._recurse_replace(v, already_passed)
            elif isinstance(v, list):
                new_dict[k] = tuple(v)
            else:
                new_dict[k] = v

        return cls(new_dict, recurse=False)


class SecretsHandler(Module):
    __slots__ = ("path", "data")

    NAMESPACE = "sec"

    def __init__(self, path: str):
        self.path = path
        self.data = None

        self.logger.info("Doing startup load!")
        self.load()  # TODO: make this whole process async so reloading
        # while using the bot doesnt block

    @staticmethod
    def _load_py(path: str, spec_name: str = "appconf"):
        if "importlib" not in dir():
            import importlib

        return importlib.util.spec_from_file_location(
            spec_name, path
        ).loader.load_module()

    @staticmethod
    def _load_json(path: str):
        if "json" not in dir():
            import json

        return BakerConfig(json.load(path))

    @staticmethod
    def _load_yaml(path: str):
        if "yaml" not in dir():
            import yaml

        with open(path, "r") as stream:
            return BakerConfig(yaml.safe_load(stream))

    @staticmethod
    def _load_toml(path: str):
        if "toml" not in dir():
            import toml

        return BakerConfig(toml.load(path))

    def _load_proper_filetype(self, path: str):
        """Chooses the correct loader to use"""
        action_decoder = {
            "py": self._load_py,
            "json": self._load_json,
            "yaml": self._load_yaml,
            "yml": self._load_yaml,
            "toml": self._load_toml,
        }

        filetype = path.split(".")[-1]

        try:
            return action_decoder[filetype](path)
        except KeyError:
            raise FileNotFoundError("Unsupported config file type!")

    def load(self):
        """Main config loading sequence"""
        self.logger.info(
            "Attempting to open the given config file at %s ...", self.path
        )
        if isfile(self.path):
            self.logger.info(
                "Successfully opened the config file. Proceeding with config data loading..."
            )
        else:
            self.logger.critical(
                "Cannot open the given config file path! Make sure you passed it correctly or if it really exists!"
            )
            raise FileNotFoundError("The config file was not found")
        try:
            self.data = self._load_proper_filetype(self.path)
        except Exception as ex:
            self.logger.critical(
                "Cannot properly load config data! Got the following error:"
            )
            raise ex

        self.logger.info("Successfully loaded config data!")

    def reload(self, data, full: bool = False):
        """
        Reloads the given data.
        It will reread the config file for this
        """
        self.load()

        # TODO

    # def data_types(self):
    #     """This is bloated and I still have to work more on this"""
    #     attrs = {
    #         {sa for sa in dir(ma) if not sa.startswith("_")}
    #         if isinstance(ma, dict)
    #         else ma
    #         for ma in dir(self.data)
    #         if not ma.startswith("_")
    #     }
    #     # ### EXPANDED ###
    #     # attrs = set()
    #     # for ma in dir(self.data):
    #     #     if not ma.startswith("_"):
    #     #         if isinstance(ma, Proxy):
    #     #             attrs.add({sa for sa in dir(ma) if not sa.startswith("_")})
    #     #             continue
    #     #             ### EXPANDED ###
    #     #             for sa in dir(ma):
    #     #                 if not sa.startswith("_"):
    #     #                     attrs.add(sa)
    #     #             ################
    #     #         attrs.add(ma)
    #     # ################
    #     return attrs


def setup(baker):
    baker.mdl.add_module(SecretsHandler(baker._secrets_path))
