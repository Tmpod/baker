#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

__all__ = ("DatabaseHandler",)

from libneko.aggregates import Proxy
from typing import Any, NewType

from . import Module


#: Dummy type representing database connections
DBConnection = NewType("DBConnection", Any)


class DBModule:
    #: The database namespace to use in the handler
    DB_NAME: str
    #: The database connection
    conn: DBConnection

    async def connect(self, db_handler: "DatabaseHandler") -> DBConnection:
        """Executed when loading the module. Should return a database connection."""
        ...

    async def disconnect(self, db_handler: "DatabaseHandler"):
        """Executed when unloading the module."""
        ...


class DatabaseHandler(Module):
    __slots__ = ("dbs", "conns")

    NAMESPACE = "dtb"

    def __init__(self):
        self.dbs = Proxy()
        self.conns = Proxy()

    def __getitem__(self, item):
        try:
            return self.conns[item]
        except KeyError:
            raise KeyError("There's no registered database with such name.")

    def __getattr__(self, attr):
        try:
            return self.__getitem__(attr)
        except KeyError:
            raise AttributeError("There's no registered database with such name.")

    async def add_db(self, db_module: DBModule) -> None:
        """Adds a database module to the handler"""
        self.logger.debug("Preparing to load `%s`...", db_module.__class__.__name__)

        name = db_module.DB_NAME
        if name in self.dbs or name in dir(self):
            raise ValueError(f"There's a database with the `{name}` namespace already!")

        conn = await db_module.connect(self)

        self.dbs[name] = db_module
        self.conns[name] = conn

        self.logger.info("Loaded `%s`!", db_module.__class__.__name__)

    async def remove_db(self, db_name: str) -> DBModule:
        """Removes a database module from the handler and returns it"""
        self.logger.debug("Preparing to unload `%s`...", db_name)

        db_module = self.dbs[db_name]
        await db_module.disconnect(self)

        self.logger.info("Unloaded `%s`!", db_module.__class__.__name__)

    def get_db(self, db_name: str) -> DBModule:
        """Gets a database module from the handler's registry"""
        return self.dbs[db_name]
