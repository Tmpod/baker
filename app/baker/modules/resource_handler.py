#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

__all__ = ("ResourceHandler",)

# import aiofiles
import aiofp
from contextlib import asynccontextmanager
from libneko import aiojson
import os
from typing import Iterable

from . import Module

# TODO: Use contextlib stuff to make this actually decent

class ResourceHandler(Module):
    """
    This handles the grabbing or posting of resources.
    It will search for a resource in the dirs specified in this module's config under the 
    `locations` namespace and handle it proply so that the interaction with resources becomes easier.
    """
    __slots__ = ("set", "resource_dirs")

    NAMESPACE = "rsc"

    def __init__(self, rsc_dirs):
        self.resource_dirs = rsc_dirs

        self.set = self.post
        # self._tpe = ThreadPoolExecutor()

    @asynccontextmanager
    async def request(
        self,
        method: str,
        resource: str,
        *,
        payload=None,
        location: str = None,
        mode: str = "r",
    ):
        method = method.upper()
        if method not in {"GET", "POST"}:
            raise TypeError("Invalid resource request method!")

        if method == "GET":
            if payload is not None:
                raise TypeError("Incompatible argument >payload< with method >GET< !")

            async with self.raw_get(resource, location, mode=mode) as fp:  # pylint: disable=E1701
                yield fp

        elif method == "POST":
            if location is None or payload is None:
                raise TypeError("Missing required arguments! (location or payload)")

            yield await self.post(resource, payload, location)

    def search(self, resource: str, locations: Iterable[str] = ()):
        """
        This will search the provided dirs (or the default ones) and look for the requested resource
        """
        def check_dir(loc: str):
            self.logger.debug(
                    "Searching location %s for %s...", loc, resource
                )
            resources = {i for i in os.listdir(loc)}
            self.logger.debug("%s contains: %s", loc, resources)
            if resource in resources:
                self.logger.debug("Resource >%s< found in >%s<! Returning path...", resource, loc)
                return f"{loc}{'/' if not loc.endswith('/') else ''}{resource}"

        self.logger.debug("Search request initiated!")

        locations = locations or self.resource_dirs.values()

        # This will make it so you can localy override repo resources
        # since the local ones take priority in the searcher
        for l in locations:
            r = check_dir(l)
            if r: 
                return r

        self.logger.error(
            "Resource >%s< not found! Raising FileNotFoundError!", resource
        )
        raise FileNotFoundError

    @asynccontextmanager
    async def raw_get(
        self, resource: str, locations: Iterable[str] = (), *, mode: str = "r"
    ):
        """
        This method searches the provided location (or all default ones if none is passsed)
        and then uses the `aiofp` module to get asynchronously open the file and deliver it
        """
        self.logger.debug("A resource grab has been requested!")
        resource_path = self.search(resource, locations)
        name_ext = resource.split(".")

        fp = aiofp.open(resource_path, mode)

        try:
            yield await fp.__aenter__()
        finally:
            await fp.close()

    async def get(self, resource: str, locations: Iterable[str] = (), *, mode: str = "r"):
        """This is just simple wrapper for the `raw_get` method that reads the requested file for you"""
        async with self.raw_get(resource, locations, mode=mode) as fp:  # pylint: disable=E1701
            return await fp.read()

            
    async def post(self, resource: str, payload: str, location: str):
        if location.lower() in ("local", "cache"):
            location = self.resource_dirs["local"]
        elif location.lower() in ("global", "persistent"):
            location = self.resource_dirs["global"]

        try:
            self.logger.debug("A resource post as been requested!")

            location, resource = self.search(resource)

        except FileNotFoundError:
            if os.path.isdir(location) is False:
                os.mkdir(location)

        finally:
            async with aiofp.open(f"{location}{resource}", "w") as fp:
                await fp.write(payload)

            self.logger.debug(
                "Successfully posted >%s< in >%s<", resource, location
            )

            return True

def setup(baker):
    baker.mdl.add_module(ResourceHandler(baker.sec.data.baker.rsc.locations))
