#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

"""
App's description
"""

#: App author
__author__ = "JohnSmith"

#: Contributors
__contributors__ = []

#: Project license
__license__ = "zlib/libpng"

#: Project title
__title__ = "DiscordBotApp"

#: App Version
__version__ = "0.0.1a"

#: Git repository URL
__repository__ = f"https://gitlab.com/{__author__}/discord-bot-app"

#: Shortcut for __repository__
__url__ = __repository__
