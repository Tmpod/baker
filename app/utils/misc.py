#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# The zlib/libpng License Copyright (c) Tmpod

# This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

# Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

# 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

# 3. This notice may not be removed or altered from any source distribution.

__all__ = ("run_in_shell", "get_files")

import asyncio
import discord
import io
import typing

async def run_in_shell(args: typing.Text) -> typing.Text:
    """Runs a command in shell"""
    # Create subprocess
    process = await asyncio.create_subprocess_shell(
        args,
        # stdout must a pipe to be accessible as process.stdout
        stdout=asyncio.subprocess.PIPE,
        # stderr=asyncio.subprocess.PIPE,
        # stdin=asyncio.subprocess.PIPE
    )
    # Wait for the subprocess to finish
    stdout, _ = await process.communicate()
    # Return stdout
    return stdout.decode().strip()
